\select@language {croatian}
\contentsline {chapter}{\numberline {1}Uvod}{1}
\contentsline {chapter}{\numberline {2}Konvolucijska neuronska mre\IeC {\v z}a}{2}
\contentsline {section}{\numberline {2.1}Motivacija}{2}
\contentsline {section}{\numberline {2.2}Slojevi konvolucijske neuronske mre\IeC {\v z}e}{4}
\contentsline {subsection}{\numberline {2.2.1}Konvolucijski sloj}{4}
\contentsline {subsection}{\numberline {2.2.2}Pooling sloj}{7}
\contentsline {subsection}{\numberline {2.2.3}Potpuno povezani sloj}{8}
\contentsline {section}{\numberline {2.3}Aktivacijska funkcija}{9}
\contentsline {subsection}{\numberline {2.3.1}ReLU}{10}
\contentsline {subsection}{\numberline {2.3.2}SoftMax}{11}
\contentsline {section}{\numberline {2.4}Kriterij pogre\IeC {\v s}ke}{12}
\contentsline {subsection}{\numberline {2.4.1}Srednji kvadrat pogre\IeC {\v s}ke (MSE)}{13}
\contentsline {subsection}{\numberline {2.4.2}Cross-Entropy}{13}
\contentsline {section}{\numberline {2.5}Algoritmi optimizacije }{15}
\contentsline {subsection}{\numberline {2.5.1}SGD}{15}
\contentsline {section}{\numberline {2.6}Treniranje mre\IeC {\v z}e}{16}
\contentsline {chapter}{\numberline {3}Pregled prakti\IeC {\v c}nog ostvarenja duboke CNN mre\IeC {\v z}e}{17}
\contentsline {subsection}{\numberline {3.0.1}Baza slika}{17}
\contentsline {subsection}{\numberline {3.0.2}YUV pretvorba}{17}
\contentsline {subsection}{\numberline {3.0.3}Normalizacija}{19}
\contentsline {subsection}{\numberline {3.0.4}Arhitektura mre\IeC {\v z}e}{22}
\contentsline {subsection}{\numberline {3.0.5}Treniranje i testiranje}{23}
\contentsline {subsection}{\numberline {3.0.6}Rezultati}{26}
\contentsline {chapter}{\numberline {4}Zaklju\IeC {\v c}ak}{31}
\contentsline {chapter}{Literatura}{32}
